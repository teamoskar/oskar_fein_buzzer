/*
   SPDX-FileCopyrightText: 2021 Johannes Strelka-Petz <johannes_at_oskars.org>
   SPDX-License-Identifier: GPL-3.0-or-later

   buzzer_main.c

   Copyright 2021 Johannes Strelka-Petz <johannes@oskars.org>

   This file is part of Oskar fein buzzer

   Oskar buzzer buzzer firmware ESP32 is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   Oskar buzzer firmware ESP32 is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with Oskar.  If not, see <https://www.gnu.org/licenses/>.
*/

/* 
   based on ESP-IDF example 
   LEDC (LED Controller) fade example
   Public Domain (or CC0 licensed, at your option.)
   from how to configure and use ESP32’s internal peripherals
*/

#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/ledc.h"
#include "driver/gpio.h"
#include "sdkconfig.h"
#include "esp_err.h"

/* Can run 'make menuconfig' to choose the GPIO to blink,
   or you can edit the following line and set a number here.
*/
//#define BLINK_GPIO CONFIG_BLINK_GPIO

//#define DOT_1_GPIO (34)
#define DOT_1_GPIO (32)
#define DOT_2_GPIO (25)
#define DOT_3_GPIO (27)
#define DOT_4_GPIO (33)
#define DOT_5_GPIO (26)
#define DOT_6_GPIO (14)
#define DOT_7_GPIO (13)
#define DOT_8_GPIO (12)

#define frequency1              (60)
#define frequency2              (240)
#define frequency3              (960)
#define frequency4              (4240)

#define DOT_GPIO_NUM           (8)
#define LEDC_LS_MODE           LEDC_LOW_SPEED_MODE

#define LEDC_TEST_DUTY         (4000)
#define DURATION1    (200)
#define DURATION2    (100)

void app_main()
{  
  int dot;

  int timers[DOT_GPIO_NUM]={LEDC_TIMER_0,LEDC_TIMER_1,LEDC_TIMER_2,LEDC_TIMER_3,LEDC_TIMER_0,LEDC_TIMER_1,LEDC_TIMER_2,LEDC_TIMER_3};
  int frequencies[DOT_GPIO_NUM]={frequency1,frequency2,frequency3,frequency4,frequency1,frequency2,frequency3,frequency4};
  // dots from first column in first half
  int gpios[DOT_GPIO_NUM]={DOT_1_GPIO,DOT_2_GPIO,DOT_3_GPIO,DOT_7_GPIO,DOT_4_GPIO,DOT_5_GPIO,DOT_6_GPIO,DOT_8_GPIO};
  int channels[DOT_GPIO_NUM]={LEDC_CHANNEL_0,LEDC_CHANNEL_1,LEDC_CHANNEL_2,LEDC_CHANNEL_3,LEDC_CHANNEL_4,LEDC_CHANNEL_5,LEDC_CHANNEL_6,LEDC_CHANNEL_7};
  ledc_channel_config_t ledc_channel[DOT_GPIO_NUM];
    /*
     * Prepare and set configuration of timers
     * that will be used by LED Controller
     */
  for (dot = 0; dot < DOT_GPIO_NUM; dot++) {
    ledc_timer_config_t ledc_timer =
      {
       .duty_resolution = LEDC_TIMER_13_BIT, // resolution of PWM duty
       .freq_hz = frequencies[dot],           // frequency of PWM signal
       .speed_mode = LEDC_LS_MODE,           // timer mode
       .timer_num = timers[dot]            // timer index
      };
      // Set configuration of timer0 for high speed channels
      ledc_timer_config(&ledc_timer);

    /*
     * Prepare individual configuration
     * for each channel of LED Controller
     * by selecting:
     * - controller's channel number
     * - output duty cycle, set initially to 0
     * - GPIO number where LED is connected to
     * - speed mode, either high or low
     * - timer servicing selected channel
     *   Note: if different channels use one timer,
     *         then frequency and bit_num of these channels
     *         will be the same
     */
      ledc_channel_config_t ledc_channel0 = {
					     .channel    = channels[dot],
					     .duty       = 0,
					     .gpio_num   = gpios[dot],
					     .speed_mode = LEDC_LS_MODE,
					     .hpoint     = 0,
					     .timer_sel  = timers[dot]
    };
      ledc_channel[dot]=ledc_channel0;
    // Set LED Controller with previously prepared configuration
    ledc_channel_config(&ledc_channel[dot]);
  }

  while(1){
    // all on
    for (dot = 0; dot < DOT_GPIO_NUM; dot++) {
      ledc_channel[dot].duty       = LEDC_TEST_DUTY/2,
      ledc_channel_config(&ledc_channel[dot]);
    }
    vTaskDelay(DURATION1 / portTICK_PERIOD_MS);

    // second halfe of channels are column 2 turn off (alterante beeping)
    for (dot = DOT_GPIO_NUM/2; dot < DOT_GPIO_NUM; dot++) {
      ledc_channel[dot].duty       = 0,
      ledc_channel_config(&ledc_channel[dot]);
    }
    vTaskDelay(DURATION2 / portTICK_PERIOD_MS);
  }
}
